//package com.kostrych.notes.config;
//
//import org.jasypt.util.password.StrongPasswordEncryptor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//
//@Configuration
//@EnableJpaRepositories("com.kostrych.notes.dao")
//public class CommonBeanConf {
//
//    @Bean
//    public StrongPasswordEncryptor strongEncryptor(){
//        StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
//        return encryptor;
//    }
//}
